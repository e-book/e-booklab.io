# Фонд METADATA

- [METADATA Foundation](https://metadata.foundation/) - центральный фонд поддержки открытых проектов. Платформа управления.
- [METADATA Cloud](https://metadata.cloud/) - облачная платформа. Содержит общие файлы, документы и другую различную информацию, предназначенную для общего пользования на всех информационных проектах.
  - [Storage](https://storage.metadata.cloud/) - общее хранилище медиа-материалов.
  - [Help](https://help.metadata.cloud/) - общая справка по всем проектам **METADATA**.